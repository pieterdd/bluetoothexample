package be.pieterdedecker.bluetoothexample.communication;

/**
 * Callback triggers when we've received new data over Bluetooth.
 * @author Pieter De Decker
 */
public interface BtIncomingDataCallback {
	public void incomingData(String text);
}
