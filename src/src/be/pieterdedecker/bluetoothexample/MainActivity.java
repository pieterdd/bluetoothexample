package be.pieterdedecker.bluetoothexample;

import static junit.framework.Assert.assertTrue;

import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import be.pieterdedecker.bluetoothexample.communication.BtClientTask;
import be.pieterdedecker.bluetoothexample.communication.BtConnectionEstablishedCallback;
import be.pieterdedecker.bluetoothexample.communication.BtConnectionTask;
import be.pieterdedecker.bluetoothexample.communication.BtIncomingDataCallback;

/**
 * This activity contains a label that shows what's going on
 * with the Bluetooth adapter.
 * @author Pieter De Decker
 */
public class MainActivity extends Activity {
	private BtConnectionTask _btCurCon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final TextView label = (TextView) findViewById(R.id.tvHelloWorld);

		// Simple Bluetooth test
		BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
		if (bluetooth == null)
			return;
		if (bluetooth.isEnabled())
			label.setText("Bluetooth enabled: " + bluetooth.getName() + " | " + bluetooth.getState());
		else
			label.setText("Bluetooth disabled");

		// Connect to first paired device
		Set<BluetoothDevice> pairedDevices = bluetooth.getBondedDevices();
		if (pairedDevices.size() > 0) {
			BluetoothDevice target = pairedDevices.iterator().next();
			assertTrue(target != null);

			BtClientTask clientTask = new BtClientTask(target);
			clientTask.execute(new BtConnectionEstablishedCallback() {
				@Override
				public void connectionEstablished(BluetoothSocket socket) {
					_btCurCon = new BtConnectionTask(socket, new BtIncomingDataCallback() {
						@Override
						public void incomingData(String text) {
							label.setText(text);
						}
					});
					_btCurCon.execute();
					_btCurCon.send("Hello world!");
				}	
			});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
